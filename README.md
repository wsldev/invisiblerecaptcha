# WebshopLogin invisible ReCAPTCHA


### Private package ###
Deze package mag alleen gebruik worden met uitdrukkelijke toestemming van Webshoplogin BV.

### Installatie ###
#### composer.json ####
	"require": {
        "php": "^7.1.3",
        "laravel/framework": "5.7.*",
        "webshoplogin/invisiblerecaptcha": "dev-master",
    },
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/wsldev/invisiblerecaptcha.git"
        }
    ],
#### Authentication ####       
Maak via bitbucket (account)settings > access management > OAuth een nieuwe customer. Geef deze customer een logische naam en beschrijving. Gebruik ook een Callback url (mag een dummy url zijn: https://webshoplogin.com). Zorg dat 'This is a private customer' aangevinkt is. Daarna bij permissions > repositories read aanvinken en klik op save.

Nu staat er onder OAuth customers een customer met key en secret. Maak in de root van het project auth.json aan. (of kopieër auth.json.example als die bestaat) Plak onderstaande code daar in en voeg key + secret in. 

       {
           "bitbucket-oauth": {
               "bitbucket.org": {
                   "consumer-key": "XXXXXX",
                   "consumer-secret": "YYYYYY"
               }
           }
       }

### Add to config/app.php

    Webshoplogin\Invisiblerecaptcha\InvisibleRecaptchaServiceProvider::class,
    
### Add keys to .env file
    WSLFRONT_INVISIBLE_RECAPTCHA_SITEKEY=YOUR_SITE_KEY_HERE
    WSLFRONT_INVISIBLE_RECAPTCHA_SECRETKEY=YOUR_SECRET_KEY_HERE
    WSLFRONT_INVISIBLE_RECAPTCHA_BADGEHIDE=true
    WSLFRONT_INVISIBLE_RECAPTCHA_DISABLE=false

### Add translation

Add this to your errors translation file:

    'captcha' => 'ReCAPTCHA is niet ingevuld.',

Execute this in your CLI:

    composer update
    php artisan vendor:publish en selecteer Webshoplogin/Invisiblerecaptcha
    
### Usage
To add the invisible recaptcha to a page, add this above the submit button:
    
    @captcha()
    
To validate if the captcha was correct use this in your controller:

    $recaptchaValidator = new RecaptchaValidation(Input::all());
    if($recaptchaValidator->isInvalid()) {
        Redirect::back()->withInput(Input::all())->with('errors', $recaptchaValidator->getErrors());
    }