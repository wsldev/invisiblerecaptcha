var _renderedTimes;
var _captchaCallback;
var _captchaForms;
var _submitForm;
var _submitBtn;
var _submitAction = true;
var _captchaForm;
var _formAction;
var _submitBtnHook;

window.onload     = function(e) {
    $.getScript('https://www.google.com/recaptcha/api.js?onload=_captchaCallback&render=explicit&hl=' + _locale).done(function(data, status, jqxhr) {
        _renderedTimes = $("._g-recaptcha").length;
        _captchaForms  = $("._g-recaptcha").closest("form");
        _captchaForms.each(function() {
            _submitBtnHook = $(this).find(":submit");
            _submitBtnHook.each(function(e) {
                this.addEventListener("click", _submitBtnHookAction)
            });
            $(this)[0].addEventListener("submit", function(e) {
                e.preventDefault();
                _captchaForm = $(this);
                _submitBtn   = $(this).find(":submit");
                grecaptcha.execute();
            });
        });
        function _submitBtnHookAction() {
            _captchaForm           = $(this).closest("form");
            _formAction            = this.formAction;
            _captchaForm[0].action = _formAction;
        }

        _submitForm = function() {
            _submitBtn.trigger("captcha");
            if(_submitAction) {
                var recaptcharesponse = document.getElementsByName("g-recaptcha-response");
                for(var i = 0; i < recaptcharesponse.length; i++) {
                    document.getElementsByName("g-recaptcha-response")[i].value = grecaptcha.getResponse();
                }
                _captchaForm.submit();
            }
        };
        _captchaCallback = function() {
            for(var x = 1; x <= _renderedTimes; x++) {
                grecaptcha.render("_g-recaptcha_" + x, {
                    sitekey : _sitekey,
                    size    : 'invisible',
                    callback: _submitForm,
                    badge : 'block'
                });
            }
        }
    })
};