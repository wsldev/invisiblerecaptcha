<?php

namespace App\Http\Validation;

use App\Http\Requests\Request;

class RecaptchaValidation extends Validation {

    protected $input;
    protected $errors = [];

    public function __construct($input) {
        $this->input = $input;
    }

    protected function validate() {
        $cValidator = new \Webshoplogin\Invisiblerecaptcha\InvisibleRecaptcha();

        //if recaptcha is disabled return true on verify
        if($cValidator->recaptchaDisabled()) {
            return true;
        }

        $cValidated = $cValidator->verifyResponse($this->input['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);

        if(!$cValidated) {
            $this->errors = trans('errors.captcha');
            return false;
        }
        return true;
    }

    public function isValid() {
        return $this->validate();
    }

    public function isInvalid() {
        return !$this->validate();
    }


    protected function getRules() {
        return [];
    }

    protected function getMessages() {
        return [];
    }

    public function getErrors() {
        return $this->errors;
    }
}