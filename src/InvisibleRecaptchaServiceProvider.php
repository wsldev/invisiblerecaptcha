<?php

namespace Webshoplogin\Invisiblerecaptcha;

use Illuminate\Support\ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\Support\Facades\Validator;

class InvisibleRecaptchaServiceProvider extends ServiceProvider
{
    /**
     * Boot the services for the application.
     * @return void
     */
    public function boot() {
        $this->bootConfig();
        $this->app['validator']->extend('captcha', function ($attribute, $value) {
            return $this->app['captcha']->verifyResponse($value, $this->app['request']->getClientIp());
        });
    }

    /**
     * Register any application services.
     * @return void
     */
    public function register() {
        $this->app->singleton('captcha', function () {
            return new InvisibleRecaptcha();
        });

        $this->app->afterResolving('blade.compiler', function () {
            $this->addBladeDirective($this->app['blade.compiler']);
        });

        $this->publishes([
            __DIR__.'/public/recaptcha.js' => public_path('js/recaptcha.js'),
        ], 'public');

        $this->publishes([
            __DIR__.'/RecaptchaValidation.php' => app_path('Http/Validation/RecaptchaValidation.php'),
        ], 'public');
    }

    /**
     * Boot configure.
     * @return void
     */
    protected function bootConfig() {
        $path = __DIR__.'/config/captcha.php';
        $this->publishes([
            $path => config_path('captcha.php'),
        ]);
    }

    /**
     * Get the services provided by the provider.
     * @return array
     */
    public function provides() {
        return ['captcha'];
    }

    /**
     * If you use @captcha in a blade this function will get called
     *
     * @param BladeCompiler $blade
     *
     * @return void
     */
    public function addBladeDirective(BladeCompiler $blade) {
        $blade->directive('captcha', function () {
            return "<?php echo app('captcha')->render(); ?>";
        });
    }
}
