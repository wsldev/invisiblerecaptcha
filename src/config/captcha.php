<?php

return [
    'siteKey'   => env('WSLFRONT_INVISIBLE_RECAPTCHA_SITEKEY'),
    'secretKey' => env('WSLFRONT_INVISIBLE_RECAPTCHA_SECRETKEY'),
    'hideBadge' => env('WSLFRONT_INVISIBLE_RECAPTCHA_BADGEHIDE', false),
    'disable'   => env('WSLFRONT_INVISIBLE_RECAPTCHA_DISABLE', false)
];