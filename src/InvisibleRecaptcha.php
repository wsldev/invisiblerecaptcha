<?php

namespace Webshoplogin\Invisiblerecaptcha;

use Illuminate\Support\Facades\App;
use Symfony\Component\HttpFoundation\Request;
use GuzzleHttp\Client;

class InvisibleRecaptcha
{
    const API_URI = 'https://www.google.com/recaptcha/api.js';
    const VERIFY_URI = 'https://www.google.com/recaptcha/api/siteverify';

    /**
     * The reCaptcha site key.
     * @var string
     */
    protected $siteKey;

    /**
     * The reCaptcha secret key.
     * @var string
     */
    protected $secretKey;

    /**
     * The config to determine if hide the badge.
     * @var boolean
     */
    protected $hideBadge;

    /**
     * The config to determine if recaptcha should be disabled
     * @var boolean
     */
    protected $disable;

    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * Rendered number in total.
     * @var integer
     */
    protected $renderedTimes = 0;

    /**
     * InvisibleReCaptcha.
     *
     * @param string  $secretKey
     * @param string  $siteKey
     * @param boolean $hideBadge
     * @param boolean $disable
     */
    public function __construct() {
        $this->siteKey   = config('captcha.siteKey');
        $this->secretKey = config('captcha.secretKey');
        $this->hideBadge = config('captcha.hideBadge');
        $this->disable   = config('captcha.disable');
        $this->client    = new Client(['timeout' => 5]);
    }

    /**
     * Get reCaptcha js by optional language param.
     *
     * @param string $lang
     *
     * @return string
     */
    public function getCaptchaJs($lang = null) {
        $apiurl = static::API_URI . '?onload=_captchaCallback&render=explicit';

        return $lang ? $apiurl . '&hl=' . $lang . '' : $apiurl . '&hl=nl';
    }

    /**
     * Render HTML reCaptcha
     *
     * @return string
     */
    public function render() {
        if($this->recaptchaDisabled()){
            return '';
        }

        $render = '';
        if($this->renderedTimes === 0) {
            $render .= "<script>var _locale = '". App::getLocale() . "';";
            $render .=  "var _sitekey = '" . $this->siteKey . "';";
            $render .= file_get_contents(public_path("/js/recaptcha.js")) . "</script>";
            if($this->hideBadge) {
                $render .= '<style>.grecaptcha-badge{display:none;!important}</style>';
            }
            $this->renderedTimes++;
        } else {
            $this->renderedTimes++;
        }
        $render .= "<div class='_g-recaptcha' id='_g-recaptcha_{$this->renderedTimes}' ></div>";

        return $render;
    }

    /**
     * Verify invisible reCaptcha response.
     *
     * @param string $response
     * @param string $clientIp
     *
     * @return mixed
     */
    public function verifyResponse($response, $clientIp)
    {
        if (empty($response)) {
            return false;
        }

        $response = $this->sendVerifyRequest([
            'secret'   => $this->secretKey,
            'remoteip' => $clientIp,
            'response' => $response
        ]);

        return isset($response['success']) && $response['success'] == true;
    }

    /**
     * Verify invisible reCaptcha response
     *
     * @param Request $request
     *
     * @return bool
     */
    public function verifyRequest(Request $request) {
        return $this->verifyResponse(
            $request->get('g-recaptcha-response'),
            $request->getClientIp()
        );
    }

    /**
     * Send verify request to google api with secretkey, remoteip and the g-recaptcha-response.
     *
     * @param array $query
     *
     * @return array
     */
    protected function sendVerifyRequest(array $query = []) {
        $response = $this->client->post(static::VERIFY_URI, [
            'form_params' => $query,
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * Check if recaptcha is disabled
     *
     * @return bool
     */
    public function recaptchaDisabled() {
        return $this->disable;
    }
}
